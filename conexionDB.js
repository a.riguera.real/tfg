const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/chatapp', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Conexión exitosa a la base de datos!'))
  .catch(err => console.error(err));

//Modelo para los usuarios de la base de datos
/*El modelo de usuario se utilizará para registrar y autenticar a los usuarios en la aplicación.*/
const esquemaUsuario = new mongoose.Schema({
  nombreUsuario: { type: String, required: true, unique: true },
  correo: { type: String, required: true, unique: true },
  contraseña: { type: String, required: true },
});

const Usuario = mongoose.model('Usuario', esquemaUsuario);



const esquemaMensaje = new mongoose.Schema({
  remitente: {
    type: String,
    required: true
  },
  contenido: {
    type: String,
    required: true
  },
  timestamp: {
    type: Date,
    default: Date.now
  },
  sala: {
    type: String,
    required: true
  }
});

const Mensaje = mongoose.model('Mensaje', esquemaMensaje);

  

  module.exports = {
    mongoose: mongoose,
    Usuario: Usuario,
    Mensaje: Mensaje
  };
  
