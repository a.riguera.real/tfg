const express = require('express');
const { mongoose, Usuario, Mensaje } = require('./conexionDB');
const bcrypt = require('bcrypt');
const socketIO = require('socket.io');
const http = require('http');
const ejs = require('ejs');
const path = require('path');
const session = require('express-session');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);
const puerto = 3000;

// Importar módulos necesarios para socket.io y sesiones
const sessionMiddleware = session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false
});

// Configuración de la aplicación Express
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: false }));
app.use(sessionMiddleware);

// Configuración del motor de plantillas EJS
app.set('view engine', 'ejs');


// Proteger rutas y requerir inicio de sesión
const requerirInicioSesion = function (req, res, next) {
  if (req.session && req.session.userId) {
    return next();
  } else {
    // Redireccionar a la página de inicio de sesión
    return res.redirect('/');
  }
};


global.salas = new Map(); // Mapa para almacenar salas y usuarios

io.on('connection', (socket) => {
  console.log('Usuario conectado:', socket.id);



  // Evento para unirse/crear sala
 socket.on('unirseCrearSala', ({ sala, usuario, socketId }) => {
  socket.join(sala); // Unirse a la sala

  // Almacenar la sala, usuario y socket ID en el mapa
  if (!global.salas.has(sala)) {
    global.salas.set(sala, [{ usuario, socketId }]);
  } else {
    const usuarios = global.salas.get(sala);
    usuarios.push({ usuario, socketId });
    global.salas.set(sala, usuarios);
  }

  console.log(global.salas);

  // Obtener los mensajes almacenados en la base de datos para la sala actual
  Mensaje.find({ sala: sala })
    .then((mensajes) => {
      // Enviar los mensajes almacenados al nuevo usuario
      socket.emit('mensajes-historial', mensajes);
    })
    .catch((error) => {
      console.error('Error al obtener los mensajes de la base de datos:', error);
    });

  // Emitir evento con la lista de usuarios conectados en la sala
  const usuariosSala = global.salas.get(sala).map((u) => u.usuario);
  io.to(sala).emit('usuarios-conectados', usuariosSala);
});








socket.on('enviar-mensaje', (sala, mensaje) => {
  // Obtener el usuario y socket ID del remitente
  const remitenteUsuario = global.salas.get(sala).find((u) => u.socketId === socket.id).usuario;

  // Emitir el evento 'nuevo-mensaje' a todos los usuarios en la misma sala
  io.to(sala).emit('nuevo-mensaje', remitenteUsuario, mensaje);

  // Crear un nuevo documento Mensaje y guardarlo en la base de datos
  const nuevoMensaje = new Mensaje({
    remitente: remitenteUsuario,
    contenido: mensaje,
    sala: sala
  });
  nuevoMensaje.save()
    .then(() => {
      console.log('Mensaje guardado en la base de datos');
    })
    .catch((error) => {
      console.error('Error al guardar el mensaje en la base de datos:', error);
    });
});













  socket.on('salir-de-sala', (sala) => {
    // Obtener la lista de usuarios de la sala
    const usuarios = global.salas.get(sala);
  
    if (usuarios) {
      // Encontrar al usuario que desea salir de la sala
      const index = usuarios.findIndex((u) => u.socketId === socket.id);
      if (index !== -1) {
        // Eliminar al usuario de la lista de usuarios de la sala
        usuarios.splice(index, 1);
        global.salas.set(sala, usuarios);
  
        // Emitir evento con la lista de usuarios conectados actualizada en la sala
        const usuariosSala = usuarios.map((u) => u.usuario);
        io.to(sala).emit('usuarios-conectados', usuariosSala);
  
        // Salir de la sala
        socket.leave(sala);
  
        // Emitir evento al cliente para limpiar la lista de contactos
        socket.emit('limpiar-contactos');
      }
    }
  });
  


  // Evento de desconexión de un cliente
  socket.on('disconnect', () => {
    console.log('Usuario desconectado:', socket.id);

    // Recorrer todas las salas para encontrar y eliminar al usuario desconectado
    for (const [sala, usuarios] of global.salas) {
      const index = usuarios.findIndex((u) => u.socketId === socket.id);
      if (index !== -1) {
        usuarios.splice(index, 1);
        global.salas.set(sala, usuarios);

        // Emitir evento con la lista de usuarios conectados actualizada en la sala
        const usuariosSala = usuarios.map((u) => u.usuario);
        io.to(sala).emit('usuarios-conectados', usuariosSala);

        break; // Salir del bucle después de encontrar al usuario y eliminarlo
      }
    }
  });
});


// Ruta para iniciar sesión
app.get('/', (req, res) => {
  res.render('login');
});

// Ruta para registrarse
app.get('/registro', (req, res) => {
  res.render('registro');
});

app.post('/registrarse', async (req, res) => {
  try {
    // Obtener los datos del formulario para registrarse
    const { nombreUsuario, correo, contraseña } = req.body;

    // Buscar en la base de datos si ya existe una cuenta con ese correo
    const existeUsuario = await Usuario.findOne({ correo: correo });

    // Si existe, enviar un mensaje de que el usuario ya existe
    if (existeUsuario) {
      res.send('El usuario ya existe. Por favor, inicie sesión.');
    } else {
      // Encriptar la contraseña con bcrypt
      const salt = await bcrypt.genSalt();
      const contraseñaEncriptada = await bcrypt.hash(contraseña, salt);

      // Crear el nuevo usuario en la base de datos
      const nuevoUsuario = new Usuario({
        nombreUsuario: nombreUsuario,
        correo: correo,
        contraseña: contraseñaEncriptada,
      });
      await nuevoUsuario.save();

      // Iniciar sesión con el nuevo usuario
      req.session.userId = nuevoUsuario._id;
      req.session.nombreUsuario = nuevoUsuario.nombreUsuario;

      // Redireccionar al chat
      res.redirect('/chat');
    }
  } catch (error) {
    console.error('Error al registrar el usuario:', error);
    res.status(500).send('Error al registrar el usuario. Por favor, inténtelo de nuevo más tarde.');
  }
});

app.post('/login', async (req, res) => {
  try {
    // Obtener los datos del formulario de login
    const { correo, contraseña } = req.body;

    // Buscar en la base de datos un usuario con el correo proporcionado
    const usuario = await Usuario.findOne({ correo: correo });

    // Verificar si el usuario existe y la contraseña es correcta
    if (usuario && await bcrypt.compare(contraseña, usuario.contraseña)) {
      // Iniciar sesión con el usuario
      req.session.userId = usuario._id;
      req.session.nombreUsuario = usuario.nombreUsuario;

      // Redireccionar al chat
      res.redirect('/chat');
    } else {
      res.send('Correo electrónico o contraseña incorrectos. Por favor, inténtelo de nuevo.');
    }
  } catch (error) {
    console.error('Error al iniciar sesión:', error);
    res.status(500).send('Error al iniciar sesión. Por favor, inténtelo de nuevo más tarde.');
  }
});

// Ruta del chat
app.get('/chat', requerirInicioSesion, (req, res) => {
  // Obtener el nombre del usuario de la sesión
  const nombre = req.session.nombreUsuario;

  // Renderizar la página de chat y pasar el nombre del usuario como parámetro
  res.render('chat', { nombre });
});

// Ruta para cerrar sesión
app.get('/logout', (req, res) => {
  // Destruir la sesión
  req.session.destroy((err) => {
    if (err) {
      console.error('Error al destruir la sesión:', err);
    }
    // Redireccionar a la página de inicio
    res.redirect('/');
  });
});

// Iniciar el servidor
server.listen(puerto, () => {
  console.log(`Servidor iniciado en http://localhost:${puerto}`);
});
