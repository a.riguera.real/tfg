const socket = io();  // Inicializar conexión con el servidor
const nombreUsuario = document.getElementById('nombreUsuario').value;
const encabezadoChat = document.getElementById('encabezado-chat');

// Obtener referencias a los elementos
var mensajeInput = document.getElementById("mensajeInput");
var enviarButton = document.querySelector(".area-input button");

// Desactivar inicialmente los elementos
mensajeInput.disabled = true;
enviarButton.disabled = true;

// Función para habilitar los elementos cuando se une a una sala
function unirseASala() {
  mensajeInput.disabled = false;
  enviarButton.disabled = false;
}

// Función para desactivar los elementos cuando se abandona la sala
function abandonarSala() {
  mensajeInput.disabled = true;
  enviarButton.disabled = true;
}

// Función para enviar el evento de unirse/crear sala al servidor
function unirseCrearSala(event) {
  event.preventDefault();
  const nombreSala = document.getElementById('nombreSalaInput').value;

  socket.emit('unirseCrearSala', { sala: nombreSala, usuario: nombreUsuario, socketId: socket.id });

  encabezadoChat.textContent = nombreSala;
  unirseASala();

  // Ocultar formulario y mostrar botón de salir de sala
  document.getElementById('salaFormulario').style.display = 'none';
  document.getElementById('salirSalaBtnAreaChat').style.display = 'block';
}

// Asignar el evento submit al formulario de unirse a la sala
document.getElementById('unirseSalaForm').addEventListener('submit', unirseCrearSala);

// Función para enviar un mensaje a los usuarios de la misma sala
function enviarMensaje() {
  const mensaje = mensajeInput.value.trim();

  if (mensaje !== '') {
    // Enviar el mensaje al servidor
    let salaActual = encabezadoChat.textContent;
    socket.emit('enviar-mensaje', salaActual, mensaje);

    // Limpiar el contenido del input de mensaje
    mensajeInput.value = '';
  }
}

// Manejar el evento 'usuarios-conectados' para actualizar la lista de contactos en la barra lateral
socket.on('usuarios-conectados', (usuarios) => {
  const listaContactos = document.getElementById('listaContactos');
  listaContactos.innerHTML = ''; // Limpiar la lista de contactos

  usuarios.forEach((usuario) => {
    if (usuario !== nombreUsuario) {
      const li = document.createElement('li');
      li.classList.add('contacto');

      const avatar = document.createElement('div');
      avatar.classList.add('avatar');

      const infoContacto = document.createElement('div');
      infoContacto.classList.add('info-contacto');

      const nombreContacto = document.createElement('div');
      nombreContacto.classList.add('nombre-contacto');
      nombreContacto.textContent = usuario;

      const ultimoMensaje = document.createElement('div');
      ultimoMensaje.classList.add('ultimo-mensaje');
      ultimoMensaje.textContent = 'Último mensaje';

      infoContacto.appendChild(nombreContacto);
      infoContacto.appendChild(ultimoMensaje);

      li.appendChild(avatar);
      li.appendChild(infoContacto);

      listaContactos.appendChild(li);
    }
  });
});

// Recibimos del servidor un nuevo mensaje
socket.on('nuevo-mensaje', (remitente, mensaje) => {
  mostrarMensaje(remitente, mensaje);
});

// Evento para recibir mensajes almacenados en la base de datos
socket.on('mensajes-historial', (mensajes) => {
  for (const mensaje of mensajes) {
    mostrarMensaje(mensaje.remitente, mensaje.contenido);
  }
});

// Función para mostrar un mensaje en la ventana de chat
function mostrarMensaje(remitente, contenido) {
  const nuevoMensaje = document.createElement('div');
  nuevoMensaje.classList.add('mensaje');

  const remitenteMensaje = document.createElement('div');
  remitenteMensaje.classList.add('remitente');
  remitenteMensaje.textContent = remitente;

  const contenidoMensaje = document.createElement('div');
  contenidoMensaje.classList.add('contenido');
  contenidoMensaje.textContent = contenido;

  nuevoMensaje.appendChild(remitenteMensaje);
  nuevoMensaje.appendChild(contenidoMensaje);

  if (remitente === nombreUsuario) {
    nuevoMensaje.classList.add('yo');
  } else {
    nuevoMensaje.classList.add('otro-usuario');
  }

  ventanaChat.appendChild(nuevoMensaje);
  ventanaChat.scrollTop = ventanaChat.scrollHeight;
}

// Función para salir de la sala actual
function salirDeSala() {
  // Limpiar la lista de contactos
  const listaContactos = document.getElementById('listaContactos');
  listaContactos.innerHTML = '';

  // Mostrar el formulario de unirse a una sala nuevamente
  document.getElementById('salaFormulario').style.display = 'block';
  document.getElementById('salirSalaBtnAreaChat').style.display = 'none';

  // Salir de la sala actual
  const salaActual = encabezadoChat.textContent;
  socket.emit('salir-de-sala', salaActual);
  abandonarSala();
}

// Asignar el evento click al botón "Salir de la sala"
document.getElementById('salirSalaBtnAreaChat').addEventListener('click', salirDeSala);

// Manejar el evento 'limpiar-contactos' para limpiar la lista de contactos y los mensajes en la ventana de chat en el cliente
socket.on('limpiar-contactos', () => {
  const listaContactos = document.getElementById('listaContactos');
  listaContactos.innerHTML = '';

  const ventanaChat = document.getElementById('ventanaChat');
  ventanaChat.innerHTML = '';
  encabezadoChat.textContent = '';
});

const nombreSalaInput = document.getElementById('nombreSalaInput');
const unirseSalaForm = document.getElementById('unirseSalaForm');
const unirseSalaBtn = unirseSalaForm.querySelector('input[type="submit"]');

// Función para verificar el estado del campo de texto y habilitar/deshabilitar el botón
function verificarCampoTexto() {
  if (nombreSalaInput.value.trim() === '') {
    unirseSalaBtn.disabled = true;
    unirseSalaBtn.style.backgroundColor = 'lightgray';
  } else {
    unirseSalaBtn.disabled = false;
    unirseSalaBtn.style.backgroundColor = '';
  }
}

// Agregar un evento de cambio al campo de texto para verificar su estado en tiempo real
nombreSalaInput.addEventListener('input', verificarCampoTexto);

// Verificar el campo de texto al cargar la página
verificarCampoTexto();